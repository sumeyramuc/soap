
### What is this repository for? ###

ToDo List Eclipse uygulamasının Android erişiminin sağlandığı bu projede; Android Studio geliştirme ortamı, Http Protokolleri,
Web Service(Restful Api) kullanılmıştır. Eclipse kullanılarak oluşturulan Todo projesinin metodlarına erişim sağlanarak
işlemler gerçekleştirilmişir.
Bu uygulamanın Eclipse geliştirmesinde Hibernate, Mvc kullanılmıştır.