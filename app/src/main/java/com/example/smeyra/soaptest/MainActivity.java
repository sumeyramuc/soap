package com.example.smeyra.soaptest;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.ColorRes;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapPrimitive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends ActionBarActivity {
    private int selectedPosition = -1;
    int choose=0;
    Button done;
    Button delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ArrayList<CheckBox> mCheckBoxes = new ArrayList<CheckBox>();
        CheckBox cb=null;
        final TableLayout tableLayout = (TableLayout) findViewById(R.id.tablelayout);
        TextView Choose=new TextView(this);
        TextView Title=new TextView(this);
        TextView Status=new TextView(this);
        TextView Logdate=new TextView(this);
        Button create = (Button) findViewById(R.id.button);
        final EditText edt = (EditText) findViewById(R.id.editText);

         done= (Button) findViewById(R.id.button2);
         delete= (Button) findViewById(R.id.button3);
         done.setEnabled(false);
         delete.setEnabled(false);

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String date= dateformat.format(new Date());


        final String link = "http://192.168.1.27:8081/DemoSpringAngularProject/getJobList.html";
        InputStream inputStream = null;
        String resultl = "";
        final String finalDate = date;
        try {

            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(link);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                //içeriği input stream` e eşitliyoruz.
                inputStream = httpResponse.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                //inputStream den string`e dönüşüm
                if (inputStream != null)
                    resultl = convertInputStreamToString(inputStream);
                else
                    resultl = "Did not work!";
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(resultl);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //create butonuna tıklanma olayı=jsonArray'e veri ekle.
        final JSONArray finalJsonarray = jsonarray;
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://192.168.1.27:8081/DemoSpringAngularProject/addJob.html");
                String json = "";
                JSONObject js=new JSONObject();

                try {
                    js.put("title", edt.getText());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    js.put("status", "UNDONE");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
// try {
//js.put("Logdate", finalDate);
// } catch (JSONException e) {
// e.printStackTrace();
//}
                json = js.toString();
                StringEntity se = null;
                try {
                    se = new StringEntity(json);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                httpPost.setEntity(se);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                Toast.makeText(getBaseContext(),json, Toast.LENGTH_LONG).show();
                finalJsonarray.put(js);

                try {
                    HttpResponse response = httpClient.execute(httpPost);
// write response to log
                    Log.d("Http Post Response:", response.toString());
                } catch (ClientProtocolException e) {
// Log exception
                    e.printStackTrace();
                } catch (IOException e) {
// Log exception
                    e.printStackTrace();
                }
            }
        });
        Drawable image=(Drawable)getResources().getDrawable(R.drawable.soap);
        TableRow row1 = new TableRow(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            row1.setBackground(image);
        }
        TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row1.setPadding(30, 30, 30, 30);
        row1.setLayoutParams(lp1);
        //sütun textleri.
        Choose.setText("Choose");
        Title.setText("Title");
        Status.setText("Status");
        Logdate.setText("Log Date");
        row1.addView(Choose);
        row1.addView(Title);
        row1.addView(Status);
        row1.addView(Logdate);
        tableLayout.addView(row1);
        //jsonArray'i parçala ve tabloya ekle
        JSONObject jsonobject = null;
            for (int i = 0; i < finalJsonarray.length(); i++) {

                final TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    row.setBackground(image);
                }
                row.setPadding(30, 30, 30, 30);

                row.setLayoutParams(lp);
                try {
                    jsonobject = finalJsonarray.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                cb = new CheckBox(getApplicationContext());
                cb.setFocusable(false);
                try {
                    cb.setId(Integer.parseInt(jsonobject.getString("id")));
                    mCheckBoxes.add(cb);
                    final JSONArray finalJsonarray1 = jsonarray;
                    cb.setOnClickListener(new View.OnClickListener() {

                        //single choice
                        @Override
                        public void onClick(View view) {
                            if (((CheckBox) view).isChecked()) {
                                for (int j = 0; j < mCheckBoxes.size(); j++) {
                                    if (mCheckBoxes.get(j) == view) {
                                        selectedPosition = j;
                                        choose=selectedPosition;
                                        try {
                                            kontrol(choose, finalJsonarray1,row);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else
                                        mCheckBoxes.get(j).setChecked(false);
                                }

                            } else {
                                selectedPosition = -1;
                            }
                        }

                    });
                    row.addView(cb);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String title = jsonobject.getString("title");
                    TextView text = new TextView(this);
                    text.setText(title);
                    text.setPadding(5,5,5,5);
                    row.addView(text);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    String status = jsonobject.getString("status");
                    TextView text = new TextView(this);
                    text.setText(status);
                    text.setPadding(5, 5, 5, 5);
                    row.addView(text);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    long createDate = jsonobject.getLong("createDate");
                    Calendar calendar = Calendar.getInstance();// tarih içerisinde belli bir günü bir değişkene atamak için Java bizlere Calendar adında bir sınıf sunar. Bu sınıf sayesinde tarih akışı içinde herhangi bir güne ulaşabiliriz.
                    TimeZone tz = TimeZone.getDefault();//zaman dilimini ifade eder
                    calendar.setTimeInMillis(createDate);
                    Date currenTimeZone = (Date) calendar.getTime();
                    TextView text = new TextView(this);
                    text.setText(dateformat.format(currenTimeZone));
                    text.setPadding(5,5,5,5);
                    row.addView(text);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                tableLayout.addView(row);


            }

        }
//güncelleme
    private void kontrol(int selectedPosition, JSONArray jsonarray, final TableRow row) throws JSONException {

        final HttpClient httpClient = new DefaultHttpClient();

        final HttpPost httpPost = new HttpPost("http://192.168.1.27:8081/DemoSpringAngularProject/updateJob.html");

        final String[] json = {""};
        JSONObject js = null;
        try {
            js = jsonarray.getJSONObject(selectedPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(selectedPosition==-1)
        {
            done.setEnabled(false);
            delete.setEnabled(false);
        }
        else {
            String k=js.getString("status").toString();
            Toast.makeText(getBaseContext(),k,Toast.LENGTH_LONG).show();
            if(k.equals("DONE"))
            {
                done.setEnabled(false);
                delete.setEnabled(true);
                final JSONObject finalJs = js;
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            finalJs.put("status", "DELETED");
                            row.setBackgroundColor(Color.parseColor("#F75D59"));
                            json[0] = finalJs.toString();
                            StringEntity se = null;
                            try {
                                se = new StringEntity(json[0]);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            httpPost.setEntity(se);
                            httpPost.setHeader("Accept", "application/json");
                            httpPost.setHeader("Content-type", "application/json");
                            //Toast.makeText(getBaseContext(),json, Toast.LENGTH_LONG).show();


                            try {
                                HttpResponse response = httpClient.execute(httpPost);
// write response to log
                                Log.d("Http Post Response:", response.toString());
                            } catch (ClientProtocolException e) {
// Log exception
                                e.printStackTrace();
                            } catch (IOException e) {
// Log exception
                                e.printStackTrace();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
            else if(k.equals("UNDONE"))
            {
                done.setEnabled(true);
                final JSONObject finalJs1 = js;
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            finalJs1.put("status", "DONE");
                            row.setBackgroundColor(Color.parseColor("#6AFB92"));
                            json[0] = finalJs1.toString();
                            StringEntity se = null;
                            try {
                                se = new StringEntity(json[0]);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            httpPost.setEntity(se);
                            httpPost.setHeader("Accept", "application/json");
                            httpPost.setHeader("Content-type", "application/json");
                            //Toast.makeText(getBaseContext(),json, Toast.LENGTH_LONG).show();


                            try {
                                HttpResponse response = httpClient.execute(httpPost);
// write response to log
                                Log.d("Http Post Response:", response.toString());
                            } catch (ClientProtocolException e) {
// Log exception
                                e.printStackTrace();
                            } catch (IOException e) {
// Log exception
                                e.printStackTrace();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                delete.setEnabled(true);
                final JSONObject finalJs2 = js;
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            finalJs2.put("status", "DELETED");
                            json[0] = finalJs2.toString();
                            StringEntity se = null;
                            try {
                                se = new StringEntity(json[0]);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            httpPost.setEntity(se);
                            httpPost.setHeader("Accept", "application/json");
                            httpPost.setHeader("Content-type", "application/json");
                            //Toast.makeText(getBaseContext(),json, Toast.LENGTH_LONG).show();


                            try {
                                HttpResponse response = httpClient.execute(httpPost);
// write response to log
                                Log.d("Http Post Response:", response.toString());
                            } catch (ClientProtocolException e) {
// Log exception
                                e.printStackTrace();
                            } catch (IOException e) {
// Log exception
                                e.printStackTrace();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
            else
            {
                done.setEnabled(false);
                delete.setEnabled(false);
            }
        }


            }


    private String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        inputStream.close();
        return result;
    }


}
